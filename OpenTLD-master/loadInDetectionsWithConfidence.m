


function [dBB, dConf, annotations] = loadInDetectionsWithConfidence(I)
    
detectThreshold = 80;

 [dBB, annotations] = readInDetections(I);
 
 dBB = dBB(dBB(:,5)>detectThreshold,:);
 
 dBB = dBB';
 size_dBB = size(dBB);
 
 
 dConf = dBB(5,:);
% Normalize confidences
 for i=1:size_dBB(2)
     dConf(i) = dConf(i) / 300;
 end
 
 
 
 dBB = dBB(1:4,:);
 

 
 for i=1:size_dBB(2)
     dBB(3,i) = dBB(3,i) + dBB(1,i);
     dBB(4,i) = dBB(4,i) + dBB(2,i);
 end
 
 dBB = round(dBB);
 
 
end