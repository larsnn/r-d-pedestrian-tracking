function [detections, annotations] = readInDetections(i)
    detections = [];
    detections_i = load(strcat(pwd,'/_input/detections/','image',num2str(i),'.mat'));
    
    detection_models = detections_i.data.det;
    annotations = detections_i.data.ann;
    
    det_size = size(detection_models);
    
    for i=1:det_size(2)
       if(strcmp(detection_models(i).name,'models/AcfCaltech+Ldcf+GPA'))
           detections = detection_models(i).bbDetections(:,1:5);
       end
    end

end
