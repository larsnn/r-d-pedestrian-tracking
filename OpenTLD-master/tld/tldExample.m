% Copyright 2011 Zdenek Kalal
%
% This file is part of TLD.
% 
% TLD is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% 
% TLD is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with TLD.  If not, see <http://www.gnu.org/licenses/>.

function [bb,conf] = tldExample(opt)

global tld; % holds results and temporal variables

% INITIALIZATION ----------------------------------------------------------

opt.source = tldInitSource(opt.source); % select data source, camera/directory

figure(2); set(2,'KeyPressFcn', @handleKey); % open figure for display of results
finish = 0; function handleKey(~,~), finish = 1; end % by pressing any key, the process will exit

while 1
    source = tldInitFirstFrame(tld,opt.source,opt.model.min_win); % get initial bounding box, return 'empty' if bounding box is too small
    if ~isempty(source), opt.source = source; break; end % check size
end

tld = tldInit(opt,[]); % train initial detector and initialize the 'tld' structure
tld = tldDisplay(0,tld); % initialize display

% RUN-TIME ----------------------------------------------------------------
i = 2;
tld.overlap(1) = 0;
tld.detections(1) = 0;
%tld.detections(2) = 0;
while i <= length(tld.source.idx) % for every frame

    
    tld = tldProcessFrame(tld,i); % process frame i
    displayTld = tldDisplay(1,tld,i); % display results on frame i
    
    overlap(i) = displayTld.overlap(i);
    detections(i) = displayTld.detections(i);
    
    if finish % finish if any key was pressed
        if tld.source.camera
            stoppreview(tld.source.vid);
            closepreview(tld.source.vid);
             close(1);
        end
        close(2);
        bb = tld.bb; conf = tld.conf; % return results
        return;
    end
    
    if tld.plot.save == 1
        img = getframe;
        imwrite(img.cdata,[tld.output num2str(i,'%05d') '.png']);
    end
        
   i = i + 1; 
end

% Evaluate

overlap_len = size(overlap);

treshold = 0.5;

for i=2:66%overlap_len(2)
    overlapBinary(i) = overlap(i) > treshold;
    detectionBinary(i) = detections(i) > treshold;
end

NumberOfCorrectTLD = sum(overlapBinary)

NumberOfCorrectDetectionsOnly = sum(detectionBinary)

bb = tld.bb; conf = tld.conf; % return results

end